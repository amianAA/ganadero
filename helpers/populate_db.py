import csv
from datetime import datetime

from ganadero.animals.models import IdentificationDate, Sheep


def load_data():

    with open('/opt/project/external/20180516.csv', 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            date = datetime.strptime(row[-1], '%d/%m/%Y')
            identif_date, date_created = IdentificationDate.objects.get_or_create(date=date)
            sheep_id = row[0]
            breed = row[1]
            gender = row[2] == 'M'
            mark = 'J' if int(sheep_id) % 2 == 0 else 'AM'

            sheep, sheep_created = Sheep.objects.get_or_create(id_code=sheep_id,
                                                breed=breed,
                                                gender=gender,
                                                mark=mark)
            sheep.identification_date.add(identif_date.id)
            sheep.save()
