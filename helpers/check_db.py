import psycopg2

status = 0

print('Waiting for connection with DB...')
while status == 0:
    try:
        connection = psycopg2.connect(host='db', user='postgres', port=5432)
        status = connection.status
    except Exception as e:
        pass

print('Connection established')
