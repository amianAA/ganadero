 FROM python:3.6
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /src
 WORKDIR /src
 COPY helpers/check_db.py /helpers/check_db.py
 ADD requirements.txt /src/
 RUN pip install -r requirements.txt
 ADD . /src/
