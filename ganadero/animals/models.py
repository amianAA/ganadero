from django.db import models


class Sheep(models.Model):
    gender = models.BooleanField()
    id_code = models.CharField(unique=True, max_length=50, null=True, blank=True)
    breed = models.CharField(max_length=20, null=True, blank=True)
    born_date = models.DateField(null=True, blank=True)
    death_date = models.DateField(null=True, blank=True)
    identification_date = models.ManyToManyField('animals.IdentificationDate')
    mark = models.CharField(max_length=5, null=True, blank=True)

    def __str__(self):
        return self.id_code


class IdentificationDate(models.Model):
    date = models.DateField(unique=True, null=False, blank=False)

    def __str__(self):
        return str(self.date)
