from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter

from ganadero.animals.api.v1_0.views import SheepViewSet

router_sheeps = SimpleRouter()
router_sheeps.register(r'sheeps', SheepViewSet, base_name='sheep')

urls = [
    url(r'', include(router_sheeps.get_urls())),
]