from rest_framework import serializers

from ganadero.animals.models import Sheep, IdentificationDate


class IdentificationDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = IdentificationDate
        fields = '__all__'


class SheepSerializer(serializers.ModelSerializer):
    identification_date = IdentificationDateSerializer(many=True)
    class Meta:
        model = Sheep
        fields = '__all__'

    def create(self, validated_data):

        if 'identification_date' in validated_data:
            try:
                id_dates = validated_data.pop('identification_date')
                valid_id_dates = self.fields['identification_date'].validate(id_dates)
                instances_id_dates = self.fields['identification_date'].create(valid_id_dates)
                ids = [instance.pk for instance in instances_id_dates]

                instance = super().create(validated_data)
                instance.identification_date = ids
                instance.save()

            except Exception as e:
                raise e

            return instance

        else:
            super().create(validated_data)


