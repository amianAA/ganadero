from rest_framework import viewsets

from ganadero.animals.api.v1_0.serializers import SheepSerializer
from ganadero.animals.models import Sheep


class SheepViewSet(viewsets.ModelViewSet):
    queryset = Sheep.objects.all()
    serializer_class = SheepSerializer
