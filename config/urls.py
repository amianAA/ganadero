from django.conf.urls import url, include
from django.contrib import admin
from django.http import JsonResponse

from .router.api_v1 import urls as api_v1


urls = [
    url(r'^__status__/', lambda _: JsonResponse({'status': 'OK'})),
    url(r'^1.0/', include(api_v1), name='api-v1'),
    url('', include(api_v1, namespace='actual-api'), name='api'),  # actual api version
]

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('api/', include(urls))
]
