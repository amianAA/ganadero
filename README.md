## Object

This project is intended to test docker with a simple app to manage a cattle farm.
UML DB diagram: https://www.draw.io/#G1kNcTWGBwKLnJxNQew9W5FeGDfL2aqDqD

## Requirements

- Docker >= 18.03.1  
- Docker-compose 1.21.2

## Set up

- Navigate to your project root folder.
- Run in command line:
- 'git clone https://github.com/amianAA/ganadero.git'
- 'docker-compose build'
- 'docker-compose up'

and it's done

## Contact info

amianalvaro@gmail.com


